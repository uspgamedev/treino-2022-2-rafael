extends Node2D

var bomb_owner;
var instantiated = false;

# Called when the node enters the scene tree for the first time.
func _ready():
	instantiated = true
	explosion_visibility()

func _on_Timer_timeout():
	bomb_owner.exploding = false
	bomb_owner.queue_free()
	queue_free()

func explosion_visibility():
	print("Oi")
	
	var raycastsMaiores = {
	"RaycastBaixo": get_node("RaycastsMaiores").get_node("RaycastBaixo"),
	"RaycastCima": get_node("RaycastsMaiores").get_node("RaycastCima"),
	"RaycastEsquerda": get_node("RaycastsMaiores").get_node("RaycastEsquerda"),
	"RaycastDireita": get_node("RaycastsMaiores").get_node("RaycastDireita")
	}
	
	var raycastsMenores = {
	"RaycastBaixo": get_node("RaycastsMenores").get_node("RaycastBaixo"),
	"RaycastCima": get_node("RaycastsMenores").get_node("RaycastCima"),
	"RaycastEsquerda": get_node("RaycastsMenores").get_node("RaycastEsquerda"),
	"RaycastDireita": get_node("RaycastsMenores").get_node("RaycastDireita")
	}
	
	if instantiated:
		for i in range(raycastsMaiores.size()):
			print("Entrou2")
			for j in range(raycastsMaiores.size()):
				raycastsMaiores.values()[i].force_raycast_update()
			if raycastsMaiores.values()[i].is_colliding():
				print('Colidindo2')
				if raycastsMaiores.values()[i].get_collider().is_in_group("paredes"):
					print("Colidindo com parede2")
					if i == 0:
						get_node("SpriteFimBaixo").queue_free()
					if i == 1:
						get_node("SpriteFimCima").queue_free()
					if i == 2:
						get_node("SpriteFimEsquerda").queue_free()
					if i == 3:
						get_node("SpriteFimDireita").queue_free()
						
		for i in range(raycastsMenores.size()):
			print("Entrou3")
			for j in range(raycastsMenores.size()):
				raycastsMenores.values()[i].force_raycast_update()
			if raycastsMenores.values()[i].is_colliding():
				print('Colidindo3')
				if raycastsMenores.values()[i].get_collider().is_in_group("paredes"):
					print("Colidindo com parede3")
					if i == 0:
						get_node("SpriteMeioBaixo").queue_free()
					if i == 1:
						get_node("SpriteMeioCima").queue_free()
					if i == 2:
						get_node("SpriteMeioEsquerda").queue_free()
					if i == 3:
						get_node("SpriteMeioDireita").queue_free()
	instantiated = false
