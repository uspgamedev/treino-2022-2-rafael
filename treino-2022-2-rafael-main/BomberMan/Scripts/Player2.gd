extends KinematicBody2D

export var speed = 50;
var life = 3;

var explosion_range = 1;
var bomb_position
var bomb_amount
var able_to_set = true
var change_range

export(PackedScene) var bomb;
export(PackedScene) var timer;
var timer_inst

func _ready():
	position = get_parent().get_node("Posicao2").position
	bomb_amount = 0

func _process(_delta):
	var velocity = Vector2.ZERO
	
	if Input.is_action_pressed("down_player2"):
		velocity.y += 1

	if Input.is_action_pressed("up_player2"):
		velocity.y -= 1

	if Input.is_action_pressed("left_player2"):
		velocity.x -= 1

	if Input.is_action_pressed("right_player2"):
		velocity.x += 1
			
	move_and_slide(velocity.normalized() * speed);
	
	if velocity.y > 0:
		$AnimatedSprite.animation = "Down"
	elif velocity.y < 0:
		$AnimatedSprite.animation = "Up"
	elif velocity.x > 0:
		$AnimatedSprite.animation = "Side"
		$AnimatedSprite.flip_h = true
	elif velocity.x < 0:
		$AnimatedSprite.animation = "Side"
		$AnimatedSprite.flip_h = false
		
	if Input.is_action_just_pressed("spawn_player2"):
		if bomb_amount < 5 and able_to_set == true:
			spawn_bomb()
			bomb_amount += 1
		
	if explosion_range == 2 and change_range == false:
		change_range = true
		timer_inst = timer.instance()
		add_child(timer_inst)
	
func spawn_bomb():
	var bomb_inst = bomb.instance()
	bomb_position = get_tilemap().world_to_map(position) * get_tilemap().get_cell_size()
	bomb_inst.position = Vector2(bomb_position) + Vector2(8,8)
	set_bomb_range(bomb_inst, explosion_range)
	bomb_inst.player = get_parent().get_node(name)
	bomb_inst.tilemap = get_tilemap()
	get_parent().get_node("Bombas").add_child(bomb_inst)

#Alcance da explosão
func set_bomb_range(bomb_inst, explosion_range):
	var up_raycast = bomb_inst.get_node("Raycast_cima").get_node("RayCast2D")
	var down_raycast = bomb_inst.get_node("Raycast_baixo").get_node("RayCast2D")
	var left_raycast = bomb_inst.get_node("Raycast_esquerda").get_node("RayCast2D")
	var right_raycast = bomb_inst.get_node("Raycast_direita").get_node("RayCast2D")
	
	up_raycast.cast_to = Vector2(0, -(16 * explosion_range + 8))
	down_raycast.cast_to = Vector2(0, (16 * explosion_range + 8))
	left_raycast.cast_to = Vector2(-(16 * explosion_range + 8), 0)
	right_raycast.cast_to = Vector2((16 * explosion_range + 8), 0)
	
	bomb_inst.bomb_range = explosion_range
	
func get_tilemap():
	return (get_parent().get_node("Grama"))


