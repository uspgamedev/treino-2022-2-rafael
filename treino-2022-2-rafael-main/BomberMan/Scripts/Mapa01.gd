extends Node2D

export(PackedScene) var tela_final

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	var popup = tela_final.instance()
	if get_node("Player1").life == 0 or get_node("Player2").life == 0:
		if get_node("Player1").life == 0:
			popup.winner = "Player 2"
		if get_node("Player2").life == 0:
			popup.winner = "Player 1"
		add_child(popup)
