extends Node2D

var bomb_owner;
var instantiated = false;


# Called when the node enters the scene tree for the first time.
func _ready():
	instantiated = true
	explosion_visibility()


func _on_Timer_timeout():
	bomb_owner.exploding = 0
	bomb_owner.queue_free()
	queue_free()
	

func explosion_visibility():
	print("Oi")
	
	var raycasts = {
	"RaycastBaixo": get_node("Raycasts").get_node("RaycastBaixo"),
	"RaycastCima": get_node("Raycasts").get_node("RaycastCima"),
	"RaycastEsquerda": get_node("Raycasts").get_node("RaycastEsquerda"),
	"RaycastDireita": get_node("Raycasts").get_node("RaycastDireita")
	}
	
	if instantiated:
		for i in range(raycasts.size()):
			print("Entrou")
			for j in range(raycasts.size()):
				raycasts.values()[i].force_raycast_update()
			if raycasts.values()[i].is_colliding():
				print('Colidindo')
				if raycasts.values()[i].get_collider().is_in_group("paredes"):
					print("Colidindo com parede")
					if i == 0:
						get_node("SpriteBaixo").queue_free()
					if i == 1:
						get_node("SpriteCima").queue_free()
					if i == 2:
						get_node("SpriteEsquerda").queue_free()
					if i == 3:
						get_node("SpriteDireita").queue_free()
	instantiated = false
