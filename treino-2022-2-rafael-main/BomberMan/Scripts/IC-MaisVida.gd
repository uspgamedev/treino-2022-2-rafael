extends Area2D
	
func _on_MaisVida_body_entered(body):
	if body.is_in_group("player"):
		if(body.life < 3):
			body.life += 1
		queue_free()
