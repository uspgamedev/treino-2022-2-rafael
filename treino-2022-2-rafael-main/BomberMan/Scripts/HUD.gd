extends CanvasLayer

export(Texture) var heart
export(Texture) var heart_desat

var life_player1 = true
var life_player2 = true
var player1
var player2

func _ready():
	player1 = get_parent().get_node("Player1")
	player2 = get_parent().get_node("Player2")

func _process(_delta):
	if (life_player1 == true) and (life_player2 == true):
		if player1.life == 3:
			$TextureRect.texture = heart
			$TextureRect2.texture = heart
			$TextureRect3.texture = heart
		if player1.life == 2:
			$TextureRect.texture = heart
			$TextureRect2.texture = heart
			$TextureRect3.texture = heart_desat
		if player1.life == 1:
			$TextureRect.texture = heart
			$TextureRect2.texture = heart_desat
			$TextureRect3.texture = heart_desat
		if player1.life == 0:
			$TextureRect.texture = heart_desat
			$TextureRect2.texture = heart_desat
			$TextureRect3.texture = heart_desat
			life_player1 = false
		if player2.life == 3:
			$TextureRect4.texture = heart
			$TextureRect5.texture = heart
			$TextureRect6.texture = heart
		if player2.life == 2:
			$TextureRect4.texture = heart
			$TextureRect5.texture = heart
			$TextureRect6.texture = heart_desat
		if player2.life == 1:
			$TextureRect4.texture = heart
			$TextureRect5.texture = heart_desat
			$TextureRect6.texture = heart_desat
		if player2.life == 0:
			$TextureRect4.texture = heart_desat
			$TextureRect5.texture = heart_desat
			$TextureRect6.texture = heart_desat
			life_player2 = false
