extends Node2D

var winner
 
func _ready():
	get_node("CanvasLayer").get_node("Label").set_text("{} Won!".format([winner], "{}"))

func _on_TextureButton_pressed():
	get_tree().change_scene("res://Cenas/Mapa01.tscn")

func _on_TextureButton2_pressed():
	get_tree().quit()
