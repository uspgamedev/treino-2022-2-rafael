extends Area2D

func _on_MenosVida_body_entered(body):
	if body.is_in_group("player"):
		if(body.life > 0):
			body.life -= 1
		queue_free()
