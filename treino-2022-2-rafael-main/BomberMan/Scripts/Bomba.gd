extends StaticBody2D

var tilemap;
var hit_player = false;
var hit_block = false;
var exploding = false;
var bomb_range;
var player;
var hit_box;

var explosion_inst;

export(PackedScene) var explosion1
export(PackedScene) var explosion2
export(PackedScene) var collision_box

func _ready():
	player.able_to_set = false
	if bomb_range == 1:
		explosion_inst = explosion1.instance()
	if bomb_range == 2:
		explosion_inst = explosion2.instance()

func _process(_delta):
	if exploding:
		explosion();
		
func _on_Timer_timeout():
	#get_node("Area2D").get_node("CollisionShape2D").disabled = true
	self.visible = false
	exploding = true
	explosion_inst.position = Vector2(position)
	explosion_inst.bomb_owner = self
	get_parent().get_parent().get_node("Explosoes").add_child(explosion_inst)
	player.bomb_amount -= 1
	

func explosion():
	explosion_to_player();
	explosion_to_block();
	
func explosion_to_player():
	var raycasts = {
	"up_raycast": get_node("Raycast_cima").get_node("RayCast2D"),
	"down_raycast": get_node("Raycast_baixo").get_node("RayCast2D"),
	"left_raycast": get_node("Raycast_esquerda").get_node("RayCast2D"),
	"right_raycast": get_node("Raycast_direita").get_node("RayCast2D")
	}
	
	if hit_player == false:
		for i in range(raycasts.size()):
			#print("Entrou1")
			for j in range(raycasts.size()):
				raycasts.values()[i].force_raycast_update()
			if raycasts.values()[i].is_colliding():			
				#print("Entrou2")
				#Verificando se um player foi atingido pelo raycast
				if raycasts.values()[i].get_collider().is_in_group("player"):
					#Destruímos o personagem na linha abaixo
					#print("Entrou3")
					hit_player = true;
					raycasts.values()[i].get_collider().life = raycasts.values()[i].get_collider().life - 1
					break
				i += 1

	
	
func explosion_to_block():
	var raycasts = {
	"up_raycast": get_node("Raycast_cima").get_node("RayCast2D"),
	"down_raycast": get_node("Raycast_baixo").get_node("RayCast2D"),
	"left_raycast": get_node("Raycast_esquerda").get_node("RayCast2D"),
	"right_raycast": get_node("Raycast_direita").get_node("RayCast2D")
	}
	
	for i in range(raycasts.size()):
		#print("Entrou1")
		for j in range(raycasts.size()):
				raycasts.values()[i].force_raycast_update()
		if raycasts.values()[i].is_colliding():			
			#print("Entrou2")
			#Verificando se um player foi atingido pelo raycast
			if raycasts.values()[i].get_collider().is_in_group("paredesQuebraveis"):
				#Destruímos o personagem na linha abaixo
				#print("Entrou3")
				raycasts.values()[i].get_collider().queue_free()
				break
			i += 1

func _on_Area2D_body_exited(body):
	print("Saiu")
	var collision_box_instance = collision_box.instance()
	call_deferred("add_child", collision_box_instance)
	player.able_to_set = true
	
