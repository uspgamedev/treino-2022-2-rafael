extends Timer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	print(self.time_left)

func _on_Timer_timeout():
	get_parent().explosion_range = 1
	get_parent().change_range = false
	queue_free()
